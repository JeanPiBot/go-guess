package main

import (
  "fmt"
  "math/rand"
  "time"
)

// Global Variables
var trys int = 0
var guess int

func guessNumber(randomNumber int) {
  trys += 1
  fmt.Println("Try #: ", trys)
  fmt.Println("Guess a number from 0 to 10: ")
  fmt.Scanf("%d", &guess)
  if guess == randomNumber {
    fmt.Println("You guessed Correctly... took you %d trys", trys)
  } else {
    fmt.Println("Guess again!")
    guessNumber(randomNumber)
  }
}


func main() {
  rand.Seed(time.Now().Unix())
  randomNumber := rand.Intn(11)
  fmt.Println("randomly generated integer: ", randomNumber)
  guessNumber(randomNumber)
}
